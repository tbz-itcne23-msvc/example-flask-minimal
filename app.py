# Ein Tutorial für die Entwicklung von MicroServices mit Python und Flask

# Schritt 1: Importiere die benötigten Module
from apiflask import APIFlask, Schema
from apiflask.fields import Float, Integer, String
from flask import jsonify, request
import requests

# Schritt 2: Erstelle eine Flask-Anwendung
app = APIFlask(__name__)

class OpIn(Schema):
    a = Float(required = True)
    b = Float(required = True)

@app.route('/')
def hello_world():
    return 'Hello ITCNE super!'


# Schritt 3: Definiere einen MicroService, der eine einfache Rechenoperation durchführt
# call service with http://localhost:5000/calc/mul?a=2.3&b=4.0
@app.route("/calc/<op>")
@app.input(OpIn, location='query')
def calc(op, query_data):
    # Hole die Parameter a und b aus der Anfrage
    a = query_data.get("a")
    b = query_data.get("b")
    # Prüfe, ob die Parameter gültig sind
    if a is None or b is None:
        return jsonify({"error": "Invalid parameters"})
    # Führe die Rechenoperation aus
    if op == "add":
        result = a + b
    elif op == "sub":
        result = a - b
    elif op == "mul":
        result = a * b
    elif op == "div":
        result = a / b
    else:
        return jsonify({"error": "Invalid operation"})
    # Gib das Ergebnis als JSON zurück
    return jsonify({"result": result})

class AmountIn(Schema):
    amount = Float(required = True)


# Schritt 4: Definiere einen MicroService, der eine Währungsumrechnung durchführt
# Aufruf mit http://localhost:5000/bitcoin/EUR?amount=0.5
@app.route("/bitcoin/<currency>")
def bitcoin(currency):
    # Hole den Parameter amount aus der Anfrage
    amount = request.args.get("amount", type=float)

    # Prüfe, ob der Parameter gültig ist
    if amount is None or amount <= 0:
        return jsonify({"error": "Invalid parameter"}), 400

    # Rufe die externe API auf, um den aktuellen Bitcoin-Kurs in USD zu erhalten
    btc_response = requests.get("https://data-api.coindesk.com/index/cc/v1/latest/tick?market=cadli&instruments=BTC-USD")

    if btc_response.status_code == 200:
        btc_data = btc_response.json()
        try:
            # Extrahiere den aktuellen Bitcoin-Kurs in USD
            btc_price_usd = btc_data["Data"]["BTC-USD"]["VALUE"]
        except KeyError:
            return jsonify({"error": "Bitcoin price data not found"}), 500
    else:
        return jsonify({"error": "Failed to fetch Bitcoin price"}), 500

    # Rufe die externe API auf, um den USD-zu-Währung Wechselkurs zu erhalten
    exchange_response = requests.get(f"https://api.exchangerate-api.com/v4/latest/USD")

    if exchange_response.status_code == 200:
        exchange_data = exchange_response.json()
        try:
            # Extrahiere den Wechselkurs zur gewünschten Währung
            exchange_rate = exchange_data["rates"].get(currency.upper())

            if exchange_rate is None:
                return jsonify({"error": "Unsupported currency"}), 400
        except KeyError:
            return jsonify({"error": "Exchange rate data not found"}), 500
    else:
        return jsonify({"error": "Failed to fetch exchange rate"}), 500

    # Berechne den umgerechneten Bitcoin-Wert in der gewünschten Währung
    btc_price_currency = btc_price_usd * exchange_rate
    result = amount * btc_price_currency

    # Gib das Ergebnis als JSON zurück
    return jsonify({
        "amount": amount,
        "currency": currency.upper(),
        "btc_price_in_currency": round(btc_price_currency, 2),
        "total_value": round(result, 2)
    })


# Schritt 5: Starte die Flask-Anwendung
if __name__ == "__main__":
    app.run(debug=True)

